using UnityEngine;

namespace VRInput.Keyboard
{
	public sealed class LayoutKey
	{
		public readonly Vector2 Pos;
		public readonly Vector2 Size;
		public readonly Texture Pressed;
		public readonly Texture Released;

		public LayoutKey (float posX, float posY, float sizeX, float sizeY, Texture pressed, Texture released)
		{
			Pos = new Vector2 (posX, posY);
			Size = new Vector2 (sizeX, sizeY);
			Pressed = pressed;
			Released = released;
		}
	}
}
