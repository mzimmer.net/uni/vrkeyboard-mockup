using System.Collections.Generic;
using UnityEngine;

namespace VRInput.Keyboard.Text
{
	public class TextMeshAdapter : ITextListener
	{
		public ICollection<string> ListeningFor { get; private set; }

		private TextMesh textMesh;

		public TextMeshAdapter (TextMesh textMesh, ICollection<string> listeningFor)
		{
			this.textMesh = textMesh;
			ListeningFor = listeningFor;
		}

		public void OnEnabledChanged (bool enabled)
		{
		}

		public void OnTextChanged (string text)
		{
			textMesh.text = text;
		}

		public void OnCursorChanged (int cursor)
		{
		}
	}
}
