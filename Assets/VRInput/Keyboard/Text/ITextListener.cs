using System.Collections.Generic;

namespace VRInput.Keyboard.Text
{
	public interface ITextListener
	{
		ICollection<string> ListeningFor { get; }

		void OnEnabledChanged (bool enabled);

		void OnTextChanged (string text);

		void OnCursorChanged (int cursor);
	}
}
