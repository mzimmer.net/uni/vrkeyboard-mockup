using System.Collections.Generic;
using UnityEngine;
using VRInput.Util;

namespace VRInput.Keyboard.Text
{
	public class TextAdapter : IListener<string, KeyEvent>
	{
		public const string DELETE_BACK_COMMAND = "[deleteback]";
		public const string DELETE_AHEAD_COMMAND = "[deleteahead]";
		public const string CURSOR_LEFT_COMMAND = "[cursorleft]";
		public const string CURSOR_RIGHT_COMMAND = "[cursorright]";
		public const string CURSOR_START_COMMAND = "[cursorstart]";
		public const string CURSOR_END_COMMAND = "[cursorend]";

		private bool enabled = false;

		public bool Enabled {
			get { return enabled; }
			set {
				if (enabled != value) {
					enabled = value;
					listener.OnEnabledChanged (enabled);
				}
			}
		}

		private string text = "";

		public string Text {
			get { return text; }
			set {
				if (text != value) {
					text = value;
					ConformCursor ();
					listener.OnTextChanged (text);
				}
			}
		}

		private int cursor = 0;

		public int Cursor {
			get { return cursor; }
			set {
				if (cursor != value) {
					cursor = value;
					ConformCursor ();
					listener.OnCursorChanged (cursor);
				}
			}
		}

		public ICollection<string> ListeningFor { get; private set; }

		private ITextListener listener;

		public TextAdapter (ITextListener listener)
		{
			this.listener = listener;
			ListeningFor = new HashSet<string> ();
			ListeningFor.Add (DELETE_BACK_COMMAND);
			ListeningFor.Add (DELETE_AHEAD_COMMAND);
			ListeningFor.Add (CURSOR_LEFT_COMMAND);
			ListeningFor.Add (CURSOR_RIGHT_COMMAND);
			ListeningFor.Add (CURSOR_START_COMMAND);
			ListeningFor.Add (CURSOR_END_COMMAND);
			foreach (string code in listener.ListeningFor)
				ListeningFor.Add (code);
		}

		public int ConformCursor (int cursor)
		{
			if (cursor < 0) {
				return 0;
			} else if (cursor > Text.Length) {
				return Text.Length;
			} else {
				return cursor;
			}
		}

		public void On (KeyEvent e)
		{
			if (Enabled && e.Type == KeyEventType.Down) {
				switch (e.Code) {
				case DELETE_BACK_COMMAND:
					if (cursor > 0) {
						string t = text.Substring (0, cursor - 1) + text.Substring (cursor, text.Length - cursor);
						cursor = cursor - 1;
						Text = t;
					}
					break;
				case DELETE_AHEAD_COMMAND:
					if (cursor < text.Length) {
						Text = text.Substring (0, cursor) + text.Substring (cursor + 1, text.Length - cursor - 1);
					}
					break;
				case CURSOR_LEFT_COMMAND:
					if (cursor > 0) {
						Cursor = cursor - 1;
					}
					break;
				case CURSOR_RIGHT_COMMAND:
					if (cursor < text.Length) {
						Cursor = cursor + 1;
					}
					break;
				case CURSOR_START_COMMAND:
					Cursor = 0;
					break;
				case CURSOR_END_COMMAND:
					Cursor = Text.Length;
					break;
				default:
					ICollection<string> listeningFor = new HashSet<string> ();
					foreach (string code in listener.ListeningFor)
						listeningFor.Add (code);
					if (listeningFor.Contains (e.Code)) {
						Text = text.Substring (0, cursor) + e.Code + text.Substring (cursor, text.Length - cursor);
						cursor = cursor + 1;
					}
					break;
				}
			}
		}

		private void ConformCursor ()
		{
			cursor = ConformCursor (cursor);
		}
	}
}
