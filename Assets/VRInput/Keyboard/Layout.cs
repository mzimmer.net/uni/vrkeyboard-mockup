using System;
using System.Collections.Generic;

namespace VRInput.Keyboard
{
	public class Layout
	{
		private IDictionary<string, LayoutKey> keys = new Dictionary<string, LayoutKey> ();

		public ICollection<string> Codes {
			get { return keys.Keys; }
		}

		public void Add (string code, LayoutKey key)
		{
			keys.Add (code, key);
		}

		public void Foreach (Action<string, LayoutKey> callback)
		{
			foreach (KeyValuePair<string, LayoutKey> key in keys) {
				callback (key.Key, key.Value);
			}
		}
	}
}
