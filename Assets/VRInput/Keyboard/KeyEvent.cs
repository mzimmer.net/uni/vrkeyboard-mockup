namespace VRInput.Keyboard
{
	public sealed class KeyEvent
	{
		public readonly string Code;
		public readonly KeyEventType Type;

		public KeyEvent (string code, KeyEventType type)
		{
			Code = code;
			Type = type;
		}
	}
}
