using UnityEngine;

namespace VRInput.Keyboard
{
	public sealed class UnityEmitterEvent
	{
		public readonly KeyCode Code;
		public readonly KeyEventType Type;

		public UnityEmitterEvent (KeyCode code, KeyEventType type)
		{
			Code = code;
			Type = type;
		}
	}
}

