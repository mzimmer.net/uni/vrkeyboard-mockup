using System.Collections.Generic;
using UnityEngine;
using VRInput.Util;

namespace VRInput.Keyboard
{
	public class UnityEmitter : MonoBehaviour, IEmitter<KeyCode, UnityEmitterEvent>
	{
		public static UnityEmitter Instance { get; private set; }

		private IDictionary<KeyCode, ICollection<IListener<KeyCode, UnityEmitterEvent>>> store = new Dictionary<KeyCode, ICollection<IListener<KeyCode, UnityEmitterEvent>>> ();

		public void AddListener (IListener<KeyCode, UnityEmitterEvent> listener)
		{
			foreach (KeyCode unityKeyCode in listener.ListeningFor) {
				ICollection<IListener<KeyCode, UnityEmitterEvent>> listeners;

				if (store.TryGetValue (unityKeyCode, out listeners)) {
					listeners.Add (listener);
				} else {
					listeners = new LinkedList<IListener<KeyCode, UnityEmitterEvent>> ();
					listeners.Add (listener);
					store.Add (unityKeyCode, listeners);
				}
			}
		}

		public void RemoveListener (IListener<KeyCode, UnityEmitterEvent> listener)
		{
			foreach (KeyValuePair<KeyCode, ICollection<IListener<KeyCode, UnityEmitterEvent>>> pair in store) {
				KeyCode unityKeyCode = pair.Key;
				ICollection<IListener<KeyCode, UnityEmitterEvent>> listeners = pair.Value;

				if (listeners.Remove (listener) && listeners.Count == 0)
					store.Remove (unityKeyCode);
			}
		}

		void Start ()
		{
			if (Instance == null)
				Instance = this;
			else
				throw new System.InvalidOperationException ("Only one UnityEmitter allowed.");
		}

		void Update ()
		{
			foreach (KeyValuePair<KeyCode, ICollection<IListener<KeyCode, UnityEmitterEvent>>> pair in store) {
				KeyCode unityKeyCode = pair.Key;
				ICollection<IListener<KeyCode, UnityEmitterEvent>> listeners = pair.Value;

				if (Input.GetKeyDown (unityKeyCode)) {
					UnityEmitterEvent e = new UnityEmitterEvent (unityKeyCode, KeyEventType.Down);

					foreach (IListener<KeyCode, UnityEmitterEvent> listener in listeners)
						listener.On (e);
				}

				if (Input.GetKeyUp (unityKeyCode)) {
					UnityEmitterEvent e = new UnityEmitterEvent (unityKeyCode, KeyEventType.Up);

					foreach (IListener<KeyCode, UnityEmitterEvent> listener in listeners)
						listener.On (e);
				}
			}
		}
	}
}
