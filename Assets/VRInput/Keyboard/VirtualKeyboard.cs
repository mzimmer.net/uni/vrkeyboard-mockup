using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRInput.Util;

namespace VRInput.Keyboard
{
	public class VirtualKeyboard : MonoBehaviour
	{
		public IListener<string, KeyEvent> Listener { get; private set; }

		public static VirtualKeyboard create (Layout layout)
		{
			GameObject GO = new GameObject ("VirtualKeyboard");
			VirtualKeyboard virtualKeyboard = GO.AddComponent<VirtualKeyboard> ();

			IDictionary<string, Mat> materials = new Dictionary<string, Mat> ();

			layout.Foreach ((code, key) => {
				GameObject keyGO = GameObject.CreatePrimitive (PrimitiveType.Plane);
				keyGO.name = code;
				Destroy (keyGO.GetComponent<Collider> ());
				Transform t = keyGO.transform;
				t.parent = GO.transform;
				t.localPosition = new Vector3(key.Pos.x + 0.5f * key.Size.x, 0.0f, -key.Pos.y - 0.5f * key.Size.y);
				t.localScale = new Vector3 (key.Size.x * 0.1f, 1.0f, key.Size.y * 0.1f);
				t.localEulerAngles = new Vector3 (0.0f, 180.0f, 0.0f);

				Material material = keyGO.GetComponent<Renderer> ().material;
				material.mainTexture = key.Released;
				material.shader = Shader.Find ("Sprites/Default");

				materials.Add (code, new Mat (material, key));
			});

			virtualKeyboard.Listener = new AsyncListener<string, KeyEvent> (() => {
				return layout.Codes;
			}, e => {
				Mat mat;
				if (materials.TryGetValue (e.Code, out mat)) {
					switch (e.Type) {
					case KeyEventType.Down:
						mat.Material.mainTexture = mat.Key.Pressed;
						break;
					case KeyEventType.Up:
						mat.Material.mainTexture = mat.Key.Released;
						break;
					}
				}
			});

			return virtualKeyboard;
		}

		private sealed class Mat
		{
			public readonly Material Material;
			public readonly LayoutKey Key;

			public Mat (Material material, LayoutKey key)
			{
				Material = material;
				Key = key;
			}
		}
	}
}
