using System;
using System.Collections.Generic;
using UnityEngine;
using VRInput.Util;

namespace VRInput.Keyboard
{
	public class UnityAdapter : AbstractAdapter<string, KeyCode, UnityEmitterEvent, KeyEvent>
	{
		private IDictionary<KeyCode, string> store;

		public ICollection<string> Codes {
			get { return store.Values; }
		}

		public UnityAdapter ()
		{
			store = new Dictionary<KeyCode, string> ();
		}

		public UnityAdapter (IDictionary<UnityEngine.KeyCode, string> mapping)
		{
			store = new Dictionary<KeyCode, string> (mapping);
		}

		public void Add (KeyCode unityKeyCode, string code)
		{
			store.Add (unityKeyCode, code);
		}

		public override ICollection<KeyCode> TransformIn (string code)
		{
			ICollection<KeyCode> unityKeyCodes = new HashSet<KeyCode> ();
			foreach (KeyValuePair<KeyCode, string> pair in store) {
				if (pair.Value == code) {
					unityKeyCodes.Add (pair.Key);
				}
			}
			return unityKeyCodes;
		}

		public override KeyEvent TransformOut (UnityEmitterEvent e)
		{
			string code;
			if (store.TryGetValue (e.Code, out code))
				return new KeyEvent (code, e.Type);
			else
				throw new KeyNotFoundException ("No mapping for KeyCode " + e.Code.ToString ());
		}

		public void ForeachMapping (Action<KeyCode, string> callback)
		{
			foreach (KeyValuePair<KeyCode, string> pair in store) {
				callback (pair.Key, pair.Value);
			}
		}
	}
}
