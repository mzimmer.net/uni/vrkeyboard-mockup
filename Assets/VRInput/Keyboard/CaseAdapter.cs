using System.Collections.Generic;
using VRInput.Util;

namespace VRInput.Keyboard
{
	public class CaseAdapter : AbstractAdapter<string, string, KeyEvent, KeyEvent>
	{
		public const string CAPSLOCK_COMMAND = "[capslock]";
		public const string LEFT_SHIFT_COMMAND = "[leftshift]";
		public const string RIGHT_SHIFT_COMMAND = "[rightshift]";

		private bool capsLock = false;
		private bool leftShift = false;
		private bool rightShift = false;

		private IDictionary<string, string> secondaryCodes;

		public CaseAdapter (IDictionary<string, string> secondaryCodes)
		{
			this.secondaryCodes = secondaryCodes;
		}

		public bool Secondary {
			get { return capsLock ? !leftShift && !rightShift : leftShift || rightShift; }
		}

		public override ICollection<string> ListeningFor {
			get {
				HashSet<string> listeningFor = new HashSet<string> ();

				listeningFor.Add (CAPSLOCK_COMMAND);
				listeningFor.Add (LEFT_SHIFT_COMMAND);
				listeningFor.Add (RIGHT_SHIFT_COMMAND);

				Foreach (listener => {
					foreach (string caseSensitiveCode in listener.ListeningFor) {
						listeningFor.Add (caseSensitiveCode);

						foreach (KeyValuePair<string, string> pair in secondaryCodes)
							if (pair.Value == caseSensitiveCode)
								listeningFor.Add (pair.Key);
					}
				});

				return listeningFor;
			}
		}

		override public void On (KeyEvent e)
		{
			switch (e.Code) {
			case CAPSLOCK_COMMAND:
				capsLock = e.Type == KeyEventType.Down;
				break;

			case LEFT_SHIFT_COMMAND:
				leftShift = e.Type == KeyEventType.Down;
				break;

			case RIGHT_SHIFT_COMMAND:
				rightShift = e.Type == KeyEventType.Down;
				break;

			default:
				string code = e.Code;
				if (Secondary)
					secondaryCodes.TryGetValue (e.Code, out code);
				base.On (new KeyEvent (code, e.Type));
				break;
			}
		}

		public override ICollection<string> TransformIn (string caseSensitiveCode)
		{
			throw new System.InvalidOperationException ();
		}

		public override KeyEvent TransformOut (KeyEvent e)
		{
			string code;
			return Secondary && secondaryCodes.TryGetValue (e.Code, out code) ? new KeyEvent (code, e.Type) : e;
		}
	}
}
