using System;
using UnityEngine;

namespace VRInput.Gaze
{
	public abstract class GazeInput : MonoBehaviour
	{
		public const float DEFAULT_MAX_DISTANCE = 100.0f;

		protected Transform source;
		protected float maxDistance;

		private Action callback;

		public static GazeInput createGazeInput (Transform source, GameObject target, Action callback, float maxDistance = DEFAULT_MAX_DISTANCE)
		{
			ImmediateGazeInput igi = target.AddComponent<ImmediateGazeInput> ();
			igi.source = source;
			igi.callback = callback;
			igi.maxDistance = maxDistance;
			return igi;
		}

		public static GazeInput createGazeInput (Transform source, GameObject target, float gazeTime, Action callback, float maxDistance = DEFAULT_MAX_DISTANCE)
		{
			WaitingGazeInput wgi = target.AddComponent<WaitingGazeInput> ();
			wgi.source = source;
			wgi.callback = callback;
			wgi.maxDistance = maxDistance;
			wgi.gazeTime = gazeTime;
			return wgi;
		}

		public void stop (bool executeCallback = true)
		{
			if (executeCallback && enabled) {
				callback ();
			}

			enabled = false;
		}
	}
}
