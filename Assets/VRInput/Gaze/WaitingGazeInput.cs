using UnityEngine;

namespace VRInput.Gaze
{

	public class WaitingGazeInput : GazeInput
	{
		public float gazeTime;

		private bool firstHit = true;
		private float gazeTimeLeft;

		void FixedUpdate ()
		{
			RaycastHit hit;

			// Create a raycast with a ray from the current position forward
			// If target is me, track gaze time
			if (Physics.Raycast (new Ray (source.position, source.forward), out hit, maxDistance) &&
			   gameObject == hit.transform.gameObject) {

				if (firstHit) {
					gazeTimeLeft = gazeTime;
					firstHit = false;
				}

				// Reduce time left to gaze
				gazeTimeLeft -= Time.deltaTime;

				// If not gazed long enough yet
				if (gazeTimeLeft > 0) {
					return;
				}

				stop ();

			} else if (!firstHit) {
				firstHit = true;
			}
		}
	}
}
