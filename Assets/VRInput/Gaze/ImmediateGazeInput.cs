using UnityEngine;

namespace VRInput.Gaze
{
	public class ImmediateGazeInput : GazeInput
	{
		void FixedUpdate ()
		{
			RaycastHit hit;

			// Create a raycast with a ray from the current position forward
			// If target is me, stop
			if (Physics.Raycast (new Ray (source.position, source.forward), out hit, maxDistance) &&
			   gameObject == hit.transform.gameObject) {
				stop ();
			}
		}
	}
}
