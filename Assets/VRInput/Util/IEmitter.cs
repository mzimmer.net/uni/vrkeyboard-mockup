namespace VRInput.Util
{
	public interface IEmitter<A, B>
	{
		void AddListener (IListener<A, B> listener);

		void RemoveListener (IListener<A, B> listener);
	}
}
