using System.Collections.Generic;

namespace VRInput.Util
{
	public abstract class AbstractAdapter<A, B, C, D> : AbstractEmitter<A, D>, IAdapter<A, B, C, D>
	{
		public abstract ICollection<B> TransformIn (A a);

		public abstract D TransformOut (C c);

		public virtual ICollection<B> ListeningFor {
			get {
				ICollection<B> bs = new HashSet<B> ();
				Foreach (listener => {
					foreach (A a in listener.ListeningFor)
						foreach (B b in TransformIn (a))
							bs.Add (b);
				});
				return bs;
			}
		}

		public virtual void On (C c)
		{
			D d = TransformOut (c);
			Foreach (listener => {
				listener.On (d);
			});
		}
	}
}
