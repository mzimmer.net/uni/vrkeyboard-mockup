using System;
using System.Collections.Generic;

namespace VRInput.Util
{
	public class AsyncListener<A, B> : IListener<A, B>
	{
		private Func<ICollection<A>> listeningFor;
		private Action<B> on;

		public AsyncListener (Func<ICollection<A>> listeningFor, Action<B> on)
		{
			this.listeningFor = listeningFor;
			this.on = on;
		}

		public void On (B b)
		{
			on (b);
		}

		public ICollection<A> ListeningFor {
			get { return listeningFor (); }
		}
	}
}
