using System.Collections.Generic;

namespace VRInput.Util
{
	public interface IListener<A, B>
	{
		ICollection<A> ListeningFor { get; }

		void On (B b);
	}
}
