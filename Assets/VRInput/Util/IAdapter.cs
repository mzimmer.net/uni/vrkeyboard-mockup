namespace VRInput.Util
{
	public interface IAdapter<A, B, C, D> : IListener<B, C>, IEmitter<A, D>
	{
	}
}
