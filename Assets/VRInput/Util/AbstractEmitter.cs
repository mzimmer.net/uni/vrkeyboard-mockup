using System;
using System.Collections.Generic;

namespace VRInput.Util
{
	public abstract class AbstractEmitter<A, B> : IEmitter<A, B>
	{
		private HashSet<IListener<A, B>> listeners = new HashSet<IListener<A, B>> ();

		public void AddListener (IListener<A, B> listener)
		{
			listeners.Add (listener);
		}

		public void RemoveListener (IListener<A, B> listener)
		{
			listeners.Remove (listener);
		}

		protected void Foreach (Action<IListener<A, B>> callback)
		{
			foreach (IListener<A, B> listener in listeners) {
				callback (listener);
			}
		}
	}
}
