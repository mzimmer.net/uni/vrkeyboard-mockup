﻿using System.Collections.Generic;
using UnityEngine;
using VRInput.Keyboard;
using VRInput.Util;

public class Recenter : MonoBehaviour
{
	public const string RECENTER_COMMAND = "[hmdviewrecenter]";

	public static Recenter Instance { get; private set; }

	private static readonly ICollection<string> LISTENING_FOR = new LinkedList<string> ();

	static Recenter ()
	{
		LISTENING_FOR.Add (RECENTER_COMMAND);
	}

	public IListener<string, KeyEvent> Listener { get; private set; }

	void Awake ()
	{
		Listener = new AsyncListener<string, KeyEvent> (() => LISTENING_FOR, e => {
			if (e.Code == RECENTER_COMMAND && e.Type == KeyEventType.Down)
				this.Try ();
		});

		if (Instance == null)
			Instance = this;
		else
			throw new System.InvalidOperationException ("Only one Recenter allowed.");
	}

	[ContextMenu ("Try to recenter")]
	public void Try ()
	{
		GvrViewer.Instance.Recenter ();
	}
}
