﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using VRInput.Keyboard;
using VRInput.Util;

public class VideoController : MonoBehaviour, IListener<string, KeyEvent>
{
	public const string PLAYPAUSE_COMMAND = "[playpause]";

	public ICollection<string> ListeningFor {
		get {
			ICollection<string> listeningFor = new LinkedList<string> ();
			listeningFor.Add (PLAYPAUSE_COMMAND);
			return listeningFor;
		}
	}

	private VideoPlayer video;

	void Start ()
	{
		video = GetComponent<VideoPlayer> ();
	}

	public void On (KeyEvent e)
	{
		if (e.Code == PLAYPAUSE_COMMAND && e.Type == KeyEventType.Down)
			PlayPause ();
	}

	public void PlayPause ()
	{
		if (video.isPlaying)
			video.Pause ();
		else
			video.Play ();
	}
}
