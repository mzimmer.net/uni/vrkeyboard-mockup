using UnityEngine;
using UnityEngine.Video;

public class Controller : MonoBehaviour
{
	public VideoController VideoController;

	public TextMesh CommentBox;
	public Keyboard Keyboard;

	void Start ()
	{
		Keyboard.CreateTextAdapter (CommentBox).Enabled = true;
		Keyboard.DirectEmitter.AddListener (VideoController);
		Keyboard.AttachSelf ();
	}
}
