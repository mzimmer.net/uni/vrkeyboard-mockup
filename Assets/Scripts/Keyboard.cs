﻿using System.Collections.Generic;
using UnityEngine;
using VRInput.Keyboard;
using VRInput.Keyboard.Text;
using VRInput.Util;

public class Keyboard : MonoBehaviour
{
	public IEmitter<string, KeyEvent> DirectEmitter{ get { return unityAdapter; } }

	private IDictionary<string, Texture> textures = new Dictionary<string, Texture> ();

	private Layout layout = new Layout ();
	private IDictionary<UnityEngine.KeyCode, string> unityKeyCodeMapping = new Dictionary<UnityEngine.KeyCode, string> ();
	private IDictionary<string, string> secondaryCases = new Dictionary<string, string> ();
	private ICollection<string> printableCodes = new LinkedList<string> ();

	private UnityAdapter unityAdapter;
	private IEmitter<string, KeyEvent> emitter;

	void Start ()
	{
		// Find all textures
		foreach (Texture texture in Resources.LoadAll<Texture>("VRInput"))
			textures.Add (texture.name, texture);

		// Add all keys to layout
		AddKeys ();

		// Create emitter
		CaseAdapter caseAdapter = new CaseAdapter (secondaryCases);
		emitter = caseAdapter;
		unityAdapter = new UnityAdapter (unityKeyCodeMapping);
		unityAdapter.AddListener (caseAdapter);

		// Create virtual keyboard from layout
		VirtualKeyboard virtualKeyboard = VirtualKeyboard.create (layout);
		virtualKeyboard.gameObject.transform.parent = transform;
		virtualKeyboard.gameObject.transform.localPosition = Vector3.zero;
		virtualKeyboard.gameObject.transform.localRotation = Quaternion.identity;
		unityAdapter.AddListener (virtualKeyboard.Listener);

		// Add VR key listener
		if (Recenter.Instance != null)
			unityAdapter.AddListener (Recenter.Instance.Listener);
	}

	public void AttachSelf ()
	{
		UnityEmitter.Instance.AddListener (unityAdapter);
	}

	public TextAdapter CreateTextAdapter (TextMesh textMesh)
	{
		TextMeshAdapter unityListener = new TextMeshAdapter (textMesh, printableCodes);
		TextAdapter textAdapter = new TextAdapter (unityListener);
		emitter.AddListener (textAdapter);
		return textAdapter;
	}

	private void AddKeys ()
	{
		float
		w = 1.0f,
		h = w * 0.9417322834645669f,
		fw = w * 14.0f / 15.0f,
		fh = 0.6f,
		x1 = 0.8f * w,
		x2 = 1.2f * w,
		x3 = 1.5f * w,
		x4 = 2 * w,
		x5 = 1 * w,
		y1 = fh + 0 * h,
		y2 = fh + 1 * h,
		y3 = fh + 2 * h,
		y4 = fh + 3 * h,
		y5 = fh + 4 * h;

		AddKey ("Esc", KeyCode.Escape, 0 * fw, 0.0f, fw, fh);
		AddKey ("F1", KeyCode.F1, 1 * fw, 0.0f, fw, fh);
		AddKey ("F2", KeyCode.F2, 2 * fw, 0.0f, fw, fh);
		AddKey ("F3", KeyCode.F3, 3 * fw, 0.0f, fw, fh);
		AddKey ("F4", KeyCode.F4, 4 * fw, 0.0f, fw, fh);
		AddKey ("F5", KeyCode.F5, 5 * fw, 0.0f, fw, fh);
		AddKey ("F6", KeyCode.F6, 6 * fw, 0.0f, fw, fh);
		AddKey ("F7", KeyCode.F7, 7 * fw, 0.0f, fw, fh);
		AddKey ("F8", KeyCode.F8, 8 * fw, 0.0f, fw, fh);
		AddKey ("F9", KeyCode.F9, 9 * fw, 0.0f, fw, fh);
		AddKey ("F10", KeyCode.F10, 10 * fw, 0.0f, fw, fh);
		AddKey ("F11", KeyCode.F11, 11 * fw, 0.0f, fw, fh);
		AddKey ("F12", KeyCode.F12, 12 * fw, 0.0f, fw, fh);
		AddKey ("Insert", KeyCode.Insert, 13 * fw, 0.0f, fw, fh);
		AddKey ("Delete", KeyCode.Delete, TextAdapter.DELETE_AHEAD_COMMAND, 14 * fw, 0.0f, fw, fh);
		AddKey ("GraveAccent", KeyCode.BackQuote, "`", "~", 0.0f, y1, x1, h, true);
		AddKey ("1", KeyCode.Alpha1, KeyCode.Exclaim, "1", "!", x1 + 0 * w, y1, w, h, true);
		AddKey ("2", KeyCode.Alpha2, KeyCode.At, "2", "@", x1 + 1 * w, y1, w, h, true);
		AddKey ("3", KeyCode.Alpha3, KeyCode.Hash, "3", "#", x1 + 2 * w, y1, w, h, true);
		AddKey ("4", KeyCode.Alpha4, KeyCode.Dollar, "4", "$", x1 + 3 * w, y1, w, h, true);
		AddKey ("5", KeyCode.Alpha5, "5", "%", x1 + 4 * w, y1, w, h, true);
		AddKey ("6", KeyCode.Alpha6, KeyCode.Caret, "6", "^", x1 + 5 * w, y1, w, h, true);
		AddKey ("7", KeyCode.Alpha7, KeyCode.Ampersand, "7", "&", x1 + 6 * w, y1, w, h, true);
		AddKey ("8", KeyCode.Alpha8, KeyCode.Asterisk, "8", "*", x1 + 7 * w, y1, w, h, true);
		AddKey ("9", KeyCode.Alpha9, KeyCode.LeftParen, "9", "(", x1 + 8 * w, y1, w, h, true);
		AddKey ("0", KeyCode.Alpha0, KeyCode.RightParen, "0", ")", x1 + 9 * w, y1, w, h, true);
		AddKey ("Minus", KeyCode.Slash, KeyCode.Question, "-", "_", x1 + 10 * w, y1, w, h, true);
		AddKey ("Equals", KeyCode.Equals, KeyCode.Plus, "=", "+", x1 + 11 * w, y1, w, h, true);
		AddKey ("Backspace", KeyCode.Backspace, TextAdapter.DELETE_BACK_COMMAND, x1 + 12 * w, y1, 2 * w - x1, h);
		AddKey ("Tab", KeyCode.Tab, 0.0f, y2, x2, h);
		AddKey ("Q", KeyCode.Q, "q", "Q", x2 + 0 * w, y2, w, h, true);
		AddKey ("W", KeyCode.W, "w", "W", x2 + 1 * w, y2, w, h, true);
		AddKey ("E", KeyCode.E, "e", "E", x2 + 2 * w, y2, w, h, true);
		AddKey ("R", KeyCode.R, "r", "R", x2 + 3 * w, y2, w, h, true);
		AddKey ("T", KeyCode.T, "t", "T", x2 + 4 * w, y2, w, h, true);
		AddKey ("Y", KeyCode.Z, "y", "Y", x2 + 5 * w, y2, w, h, true);
		AddKey ("U", KeyCode.U, "u", "U", x2 + 6 * w, y2, w, h, true);
		AddKey ("I", KeyCode.I, "i", "I", x2 + 7 * w, y2, w, h, true);
		AddKey ("O", KeyCode.O, "o", "O", x2 + 8 * w, y2, w, h, true);
		AddKey ("P", KeyCode.P, "p", "P", x2 + 9 * w, y2, w, h, true);
		AddKey ("BracketOpen", KeyCode.LeftBracket, "[", "{", x2 + 10 * w, y2, w, h, true);
		AddKey ("BracketClose", KeyCode.RightBracket, "]", "}", x2 + 11 * w, y2, w, h, true);
		AddKey ("Backslash", KeyCode.Backslash, "\\", "|", x2 + 12 * w, y2, 2 * w - x2, h, true);
		AddKey ("CapsLock", KeyCode.CapsLock, CaseAdapter.CAPSLOCK_COMMAND, 0.0f, y3, x3, h);
		AddKey ("A", KeyCode.A, "a", "A", x3 + 0 * w, y3, w, h, true);
		AddKey ("S", KeyCode.S, "s", "S", x3 + 1 * w, y3, w, h, true);
		AddKey ("D", KeyCode.D, "d", "D", x3 + 2 * w, y3, w, h, true);
		AddKey ("F", KeyCode.F, "f", "F", x3 + 3 * w, y3, w, h, true);
		AddKey ("G", KeyCode.G, "g", "G", x3 + 4 * w, y3, w, h, true);
		AddKey ("H", KeyCode.H, "h", "H", x3 + 5 * w, y3, w, h, true);
		AddKey ("J", KeyCode.J, "j", "J", x3 + 6 * w, y3, w, h, true);
		AddKey ("K", KeyCode.K, "k", "K", x3 + 7 * w, y3, w, h, true);
		AddKey ("L", KeyCode.L, "l", "L", x3 + 8 * w, y3, w, h, true);
		AddKey ("Semicolon", KeyCode.Semicolon, KeyCode.Colon, ";", ":", x3 + 9 * w, y3, w, h, true);
		AddKey ("Apostrophe", KeyCode.Quote, KeyCode.DoubleQuote, "'", "\"", x3 + 10 * w, y3, w, h, true);
		AddKey ("Enter", KeyCode.Return, x3 + 11 * w, y3, 3 * w - x3, h);
		AddKey ("ShiftL", KeyCode.LeftShift, CaseAdapter.LEFT_SHIFT_COMMAND, 0.0f, y4, x4, h);
		AddKey ("Z", KeyCode.Y, "z", "Z", x4 + 0 * w, y4, w, h, true);
		AddKey ("X", KeyCode.X, "x", "X", x4 + 1 * w, y4, w, h, true);
		AddKey ("C", KeyCode.C, "c", "C", x4 + 2 * w, y4, w, h, true);
		AddKey ("V", KeyCode.V, "v", "V", x4 + 3 * w, y4, w, h, true);
		AddKey ("B", KeyCode.B, "b", "B", x4 + 4 * w, y4, w, h, true);
		AddKey ("N", KeyCode.N, "n", "N", x4 + 5 * w, y4, w, h, true);
		AddKey ("M", KeyCode.M, "m", "M", x4 + 6 * w, y4, w, h, true);
		AddKey ("Comma", KeyCode.Comma, KeyCode.Less, ",", "<", x4 + 7 * w, y4, w, h, true);
		AddKey ("Dot", KeyCode.Period, KeyCode.Greater, ".", ">", x4 + 8 * w, y4, w, h, true);
		AddKey ("PlayPause", KeyCode.Minus, VideoController.PLAYPAUSE_COMMAND, x4 + 9 * w, y4, w, h);
		AddKey ("Up", KeyCode.UpArrow, x4 + 10 * w, y4, w, h);
		AddKey ("Smiley", KeyCode.RightShift, "☺", x4 + 11 * w, y4, w, h, true);
		AddKey ("CtrlL", KeyCode.LeftControl, 0.0f, y5, x5, h);
		AddKey ("Windows", KeyCode.LeftWindows, x5 + 1 * w, y5, w, h);
		AddKey ("AltL", KeyCode.LeftAlt, x5 + 2 * w, y5, w, h);
		AddKey ("Space", KeyCode.Space, " ", x5 + 3 * w, y5, 5 * w, h, true);
		AddKey ("AltR", KeyCode.RightAlt, x5 + 8 * w, y5, w, h);
		AddKey ("Cardboard", KeyCode.RightControl, Recenter.RECENTER_COMMAND, x5 + 9 * w, y5, w, h);
		AddKey ("Left", KeyCode.LeftArrow, TextAdapter.CURSOR_LEFT_COMMAND, x5 + 10 * w, y5, w, h);
		AddKey ("Down", KeyCode.DownArrow, x5 + 11 * w, y5, w, h);
		AddKey ("Right", KeyCode.RightArrow, TextAdapter.CURSOR_RIGHT_COMMAND, x5 + 12 * w, y5, w, h);
		AddKey (KeyCode.Home, TextAdapter.CURSOR_START_COMMAND);
		AddKey (KeyCode.End, TextAdapter.CURSOR_END_COMMAND);
		AddKey (KeyCode.Break, CaseAdapter.RIGHT_SHIFT_COMMAND); // placeholder - replaced by smiley
	}

	private void AddKey (KeyCode unityKeyCode, string code)
	{
		unityKeyCodeMapping.Add (unityKeyCode, code);
	}

	private void AddKey (string texture, KeyCode unityKeyCode, float posX, float posY, float scaleX, float scaleY)
	{
		AddKey (texture, unityKeyCode, "[withoutcode:" + texture + "]", posX, posY, scaleX, scaleY);
	}

	private void AddKey (string texture, KeyCode unityKeyCode, string code, float posX, float posY, float scaleX, float scaleY, bool printable = false)
	{
		Texture pressedTexture;
		Texture releasedTexture;
		textures.TryGetValue ("VRInput.Keyboard.K400." + texture + ".Pressed", out pressedTexture);
		textures.TryGetValue ("VRInput.Keyboard.K400." + texture + ".Released", out releasedTexture);
		if (pressedTexture == null || releasedTexture == null)
			throw new System.InvalidOperationException ("Could not find texture for " + code + "!");
		layout.Add (code, new LayoutKey (posX, posY, scaleX, scaleY, pressedTexture, releasedTexture));
		unityKeyCodeMapping.Add (unityKeyCode, code);
		if (printable)
			printableCodes.Add (code);
	}

	private void AddKey (string texture, KeyCode unityKeyCode, string codePrimaryCase, string codeSecondaryCase, float posX, float posY, float scaleX, float scaleY, bool printable = false)
	{
		AddKey (texture, unityKeyCode, codePrimaryCase, posX, posY, scaleX, scaleY, printable);
		secondaryCases.Add (codePrimaryCase, codeSecondaryCase);
		if (printable)
			printableCodes.Add (codeSecondaryCase);
	}

	private void AddKey (string texture, KeyCode unityKeyCode1, KeyCode unityKeyCode2, string codePrimaryCase, string codeSecondaryCase, float posX, float posY, float scaleX, float scaleY, bool printable = false)
	{
		AddKey (texture, unityKeyCode1, codePrimaryCase, codeSecondaryCase, posX, posY, scaleX, scaleY, printable);
		unityKeyCodeMapping.Add (unityKeyCode2, codePrimaryCase);
	}
}
