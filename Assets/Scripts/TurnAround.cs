﻿using UnityEngine;

public class TurnAround : MonoBehaviour
{
	public float Speed = 0.0f;

	void Update ()
	{
		transform.Rotate (Vector3.up, Speed * Time.deltaTime);
	}
}
