# Typing in Virtual Reality

## From the corresponding paper
Head-mounted displays provide a great opportunity to shield the user from the real world and immerse it further into the virtual environment. A problem with this level of occlusion is that input devices like mouse and keyboard are no longer visible to the immersed user and interactions like typing become a problem. We developed a prototype to display a representation of the physical keyboard in the virtual environment. We also provide feedback by virtually highlighting pressed keys.

## vrkeyboard-mockup
This repository contains the prototype developed in the [Unity Game Engine](https://unity3d.com/) using the [Google VR SDK for Unity ](https://developers.google.com/vr/unity/). Version 5.6 of Unity is required. The Google VR SDK is included.
The APK can be built inside Unity by selecting Android in the Build Settings menu and it can be previewed in Unity Editor as any other Unity project by pressing play.
